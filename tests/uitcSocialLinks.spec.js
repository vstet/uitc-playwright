// Generate test for social links using Playwright codegen

import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://uitc.space/');
  // Header
  // const page1Promise = page.waitForEvent('popup');
  await page.locator('#header').getByRole('link', { name: 'Instagram' }).click();
  // const page1 = await page1Promise;
  await expect(page).toHaveURL(/.*uitc.space/); 
  
  /* const page2Promise = page.waitForEvent('popup');
  await page.locator('#header').getByRole('link', { name: 'Telegram' }).click();
  const page2 = await page2Promise;
  const page3Promise = page.waitForEvent('popup');
  await page.locator('#header').getByRole('link', { name: 'Facebook' }).click();
  const page3 = await page3Promise;
  const page4Promise = page.waitForEvent('popup');
  await page.locator('#header').getByRole('link', { name: 'Twitter' }).click();
  const page4 = await page4Promise;
  const page5Promise = page.waitForEvent('popup');
  await page.locator('#header').getByRole('link', { name: 'LinkedIn' }).click();
  const page5 = await page5Promise;
  const page6Promise = page.waitForEvent('popup');
  // Fotoer
  await page.locator('#footer').getByRole('link', { name: 'Instagram' }).click();
  const page6 = await page6Promise;
  const page7Promise = page.waitForEvent('popup');
  await page.locator('#footer').getByRole('link', { name: 'Telegram' }).click();
  const page7 = await page7Promise;
  const page8Promise = page.waitForEvent('popup');
  await page.locator('#footer').getByRole('link', { name: 'Facebook' }).click();
  const page8 = await page8Promise;
  const page9Promise = page.waitForEvent('popup');
  await page.locator('#footer').getByRole('link', { name: 'Twitter' }).click();
  const page9 = await page9Promise;
  const page10Promise = page.waitForEvent('popup');
  await page.locator('#footer').getByRole('link', { name: 'LinkedIn' }).click();
  const page10 = await page10Promise;
  await page10.getByRole('button', { name: 'Dismiss' }).click(); */
});