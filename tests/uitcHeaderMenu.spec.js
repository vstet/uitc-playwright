// @ts-check
const { test, expect } = require('@playwright/test');

const uitcLink = 'https://uitc.space';
// CSS selectors
const about = '#menu-item-14 > a:nth-child(1)';
const events = '#menu-item-16 > a:nth-child(1)';
const ourTeam = '#menu-item-62 > a:nth-child(1)';
const contacts = '#menu-item-17';

test('Title is right', async ({ page }) => {
  await page.goto(uitcLink);

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle('UITC – Ukrainian IT Cluster');
});

test('Check header menu', async ({ page }) => {
  await page.goto(uitcLink);

  await page.locator(about).click();
  await expect(page).toHaveURL(/.*#about-ukrainian-it-cluster/);

  await page.locator(events).click();
  await expect(page).toHaveURL(/.*#events/);

  await page.locator(ourTeam).click();
  await expect(page).toHaveURL(/.*#our-team/);

  await page.locator(contacts).click();
  await expect(page).toHaveURL(/.*#contacts/);

});
